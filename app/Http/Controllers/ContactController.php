<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Models\Contacts;

class ContactController extends Controller
{
    public function submit(ContactRequest $request)
    {
        $contact = new Contacts();
        $contact->name = $request->input('name');
        $contact->email = $request->input('email');
        $contact->subject = $request->input('subject');
        $contact->message = $request->input('message');
        $contact->save();

        return redirect()->route('home')->with('success', 'Сообщение бвло добавлено');
    }

    public function allData()
    {
        // $contact = new Contacts;
        // dd($contact->all());
        // return view('messages', ['data' => $contact->inRandomOrder()->get()]);
        // return view('messages', ['data' => $contact->orderBy('id', 'desc')->take(2)->get()]);
        // return view('messages', ['data' => $contact->where('subject', '=', 'Hello')->get()]);

        return view('messages', ['data' => Contacts::all()]);
    }
    
    public function showOneMessage($id)
    {
        $contact = new Contacts;
        return view('one-message', ['data' => $contact->find($id)]);
    }
    
    public function updateMessage($id)
    {
        $contact = new Contacts;
        return view('update-message', ['data' => $contact->find($id)]);
    }

    public function updateMessageSubmit($id, ContactRequest $request)
    {
        $contact = Contacts::find($id);
        $contact->name = $request->input('name');
        $contact->email = $request->input('email');
        $contact->subject = $request->input('subject');
        $contact->message = $request->input('message');
        $contact->save();

        return redirect()->route('contact-data-one', $id)->with('success', 'Сообщение было обновлено');
    }


    public function deleteMessage($id)
    {
        Contacts::find($id)->delete();
        return redirect()->route('contact-data')->with('success', 'Сообщение было удалено');
    }
    

    // public function submit(Request $request)
    // {
    // В случае с стандартным Request
    //     $validation = $request->validate([
    //        'subject' => 'required|min:5|max:50',
    //        'message' => 'required|min:5|max:500'
    //     ]);

    //     dd($request->input('subject'));
    //     return $request->all();
    // }
}
