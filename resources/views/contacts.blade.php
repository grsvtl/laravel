@extends('layouts.app')

@section('title-meta')
Contacts
@endsection

@section('content')
<h1>Contacts</h1>


@if($errors->any())
<div class="alert alert-danger">
 <ul>

    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach

 </ul>
</div>
@endif

<br/>
<form action="{{ route('contact-form') }}" method="post">
    @csrf
    {{ csrf_field() }}
    <div class="form-group">
        <label for="name">Введите Имя</label>
        <input type="text" name="name" value="" id="name" class="form-control">
    </div>
    <div class="form-group">
        <label for="email">Введите Email</label>
        <input type="text" name="email" value="" id="email" class="form-control">
    </div>
    <div class="form-group">
        <label for="subject">Введите Тему</label>
        <input type="text" name="subject" value="" id="subject" class="form-control">
    </div>
    <div class="form-group">
        <label for="message">Сообщение</label>
        <textarea name="message" id="message" class="form-control"></textarea>
    </div>
    <button type="submit" name="submit" value="Отправить" class="btn btn-success">Отправить</button>
</form>

@endsection