@section('header')
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
  <h5 class="my-0 mr-md-auto font-weight-normal">Company name</h5>
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark" href="{{ url('/') }}">Home</a>
	<a class="p-2 text-dark" href="{{ url('/about') }}">About</a>
    <a class="p-2 text-dark" href="{{ url('/contacts') }}">Contacts</a>
    <a class="p-2 text-dark" href="{{ route('about') }}">About</a>
    <a class="p-2 text-dark" href="{{ route('contacts') }}">Contacts</a>
    <a class="p-2 text-dark" href="{{ route('contact-data') }}">ContactsData</a>
  </nav>
  
	@if (Route::has('login'))
		@auth
			<a href="{{ url('/home') }}" class="btn btn-outline-primary">{{ auth::user()->name }}</a>
			<a href="{{ route('logout') }}" class="btn btn-outline-danger">Logout</a>
		@else
			<a href="{{ route('login') }}" class="btn btn-outline-primary">Login</a>

			@if (Route::has('register'))
				<a href="{{ route('register') }}" class="btn btn-outline-success">Register</a>
			@endif
		@endif
	@endif
  
</div>