@extends('layouts.app')

@section('title-meta')
Home
@endsection

@section('content')
<h1>Hello <?= $name ?></h1>
@endsection

@section('aside')
	@parent
	<p>Показ доп контента</p>
@endsection