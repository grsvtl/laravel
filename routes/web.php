<?php

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/contacts', function () {
    return view('contacts');
})->name('contacts');


Route::get('/hello', function () {
    return view('hello', [
        'name' => 'Grosu',
    ]);
})->name('hello');


Route::get(
    '/contact/all/{id}',
    'ContactController@showOneMessage'
)->name('contact-data-one');

Route::get(
    '/contact/all/{id}/update',
    'ContactController@updateMessage'
)->name('contact-update');

Route::post(
    '/contact/all/{id}/update',
    'ContactController@updateMessageSubmit'
)->name('contact-update-submit');

Route::get(
    '/contact/all/{id}/delete',
    'ContactController@deleteMessage'
)->name('contact-delete');

Route::get('/contact/all', 'ContactController@allData')->name('contact-data');
Route::post('/contact/submit', 'ContactController@submit')->name('contact-form');


// Route::post('/contact/submit', function () {
//     return Request::all();
// })->name('contact-form');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');