<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Document</title>
</head>
<body>
	<div class="flex-center position-ref full-height">

		<div class="top-right links">
			<a href="{{ url('/home') }}">Home</a>
			<a href="{{ url('/about') }}">About</a>
			<a href="{{ url('/contacts') }}">Contacts</a>
		</div>

		<div class="content">
			<div class="title m-b-md">
				Home
			</div>
		</div>
	</div>
</body>
</html>
